//
//  DatabaseManager.swift
//  SqlFMDBDemo
//
//  Created by James Cash on 12-02-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

import Foundation
import FMDB

struct Student {
    let id: Int32
    let name: String
    let funFact: String?
}

struct Mark {
    let id: Int32
    let mark: Int32
    // maybe have a reference to the student too?
}

class DatabaseManager {
    let db: FMDatabase

    init() {
        let docsURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        let dbURL = docsURL.appendingPathComponent("database.sqlite")
        db = FMDatabase(url: dbURL)
        if !db.open() {
            print("Couldn't open database")
            abort()
        }
        createSchema()
        print("Created db at \(dbURL)")
        if countStudents() == 0 {
            seedStudents()
        }
    }

    deinit {
        db.close()
    }

    func createSchema() {
        let query = """
CREATE TABLE IF NOT EXISTS students (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  name text not null,
  fun_fact text
);

CREATE TABLE IF NOT EXISTS marks (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  mark INTEGER NOT NULL,
  student_id integer not null,
  FOREIGN KEY(student_id) REFERENCES students(id)
);
"""
        if !db.executeStatements(query) {
            print("Couldn't create schema")
            abort()
        }
    }

    func countStudents() -> Int {
        let query = "SELECT COUNT(*) AS count FROM students;"
        do {
            let results: FMResultSet = try db.executeQuery(query, values: nil)
            results.next()
            return Int(results.int(forColumn: "count"))
        } catch let err {
            print("Error getting students count: \(err)")
            return 0
        }
    }

    func seedStudents() {
        let query = """
INSERT INTO students (name, fun_fact)
VALUES
('James', 'Sleepy'),
('Hello', 'Odd name'),
('snths', NULL),
('παβγθ', 'Hard to pronounce name');
"""
        do {
            try db.executeUpdate(query, values: nil)
        } catch let err {
            print("Error seeding: \(err)")
            return
        }
    }

    func add(mark: Int, for student: Student) {
        let query = "INSERT INTO marks (student_id, mark) VALUES (:student_id, :mark);"
        db.executeUpdate(
            query,
            withParameterDictionary: ["student_id": student.id,
                                      "mark": mark])
    }

    func countMarks() -> Int {
        let r = try! db.executeQuery("SELECT COUNT(*) as count from marks;", values: nil)
        r.next()
        return Int(r.int(forColumn: "count"))
    }

    func allStudents() -> [Student] {
        let query = "SELECT * FROM students ORDER BY id ASC;"
        do {
            let results = try db.executeQuery(query, values: nil)
            var students = [Student]()
            while results.next() {
                students.append(
                    Student(id: results.int(forColumn: "id"),
                            name: results.string(forColumn: "name")!,
                            funFact: results.string(forColumn: "fun_fact"))
                )
            }
            return students
        } catch let err {
            print("Error selecting all: \(err)")
            abort()
        }
    }

    // DO NOT DO THIS!!!!!!!!!!!
//    func insertStudent(name: String, funFact: String?) {
//        let query = """
//INSERT INTO students (name, fun_fact) VALUES ('\(name)', '\(funFact)');
//"""
//        if !db.executeStatements(query) {
//            print("Error inserting student")
//            abort()
//        }
//    }

    func insertStudent(name: String, funFact: String?) {
        let query = """
INSERT INTO students (name, fun_fact) VALUES (:name, :fact);
"""
        if !db.executeUpdate(
                query,
                withParameterDictionary: ["name": name,
                                          "fact": funFact as Any]) {
            print("Error inserting student")
            abort()
        }
    }

    func marksFor(student: Student) -> [Mark] {
        let query = """
SELECT * FROM marks WHERE student_id = :student_id;
"""
        let r = db.executeQuery(query, withParameterDictionary: ["student_id": student.id])!
        var marks = [Mark]()
        while r.next() {
            marks.append(Mark(id: r.int(forColumn: "id"),
                              mark: r.int(forColumn: "mark")))
        }
        return marks
    }

    func search(name: String) -> [(String, Double)] {
        let query = """
SELECT students.id, students.name as name, avg(marks.mark) as average
FROM students
LEFT JOIN marks ON marks.student_id = students.id
WHERE students.name LIKE '%' || :name || '%'
GROUP BY students.id;
"""
        let r = db.executeQuery(query, withParameterDictionary: ["name": name])!

        var info = [(String, Double)]()
        while r.next() {
            info.append((r.string(forColumn: "name")!,
                         r.double(forColumn: "average")))
        }
        return info
    }
}
