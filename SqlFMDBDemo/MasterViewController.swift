//
//  MasterViewController.swift
//  SqlFMDBDemo
//
//  Created by James Cash on 12-02-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

import UIKit

class MasterViewController: UITableViewController {

    var detailViewController: DetailViewController? = nil
    var objects = [Student]()

    let dbManager = DatabaseManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        navigationItem.leftBarButtonItem = editButtonItem

        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(insertNewObject(_:)))
        navigationItem.rightBarButtonItem = addButton
        if let split = splitViewController {
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }

//        dbManager.insertStudent(name: "', 'foo'); DELETE FROM students; --", funFact: "Mother was a DB admin")
        let studentsCount = dbManager.countStudents()
        print("There are currently \(studentsCount) students in the db")
        let students = dbManager.allStudents()
        if dbManager.countMarks() == 0 {
            for student in students {
                let nMarks = arc4random_uniform(5)
                for i in 0..<nMarks {
                    dbManager.add(mark: Int(arc4random_uniform(100)),
                                  for: student)
                }
            }
        }
        print("Students: \(students)")
        objects = students
        tableView.reloadData()

        print("Searching")
        let searchRes = dbManager.search(name: "foo")
        print("results: \(searchRes)")
    }

    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }

    @objc
    func insertNewObject(_ sender: Any) {
        // exercise for reader: make an alertviewcontroller or something with fields
        dbManager.insertStudent(name: "foo \(arc4random())", funFact: "Random person")
        objects = dbManager.allStudents()
        let indexPath = IndexPath(row: objects.count - 1, section: 0)
        tableView.insertRows(at: [indexPath], with: .automatic)
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let object = objects[indexPath.row]
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
                controller.db = dbManager
                controller.detailItem = object
                controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objects.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        let object = objects[indexPath.row]
        cell.textLabel!.text = object.name
//        cell.detailTextLabel!.text = object.funFact ?? "No fact"
        return cell
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

//    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
//        if editingStyle == .delete {
//            objects.remove(at: indexPath.row)
//            tableView.deleteRows(at: [indexPath], with: .fade)
//        } else if editingStyle == .insert {
//            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
//        }
//    }


}

