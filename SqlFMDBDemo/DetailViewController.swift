//
//  DetailViewController.swift
//  SqlFMDBDemo
//
//  Created by James Cash on 12-02-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var detailDescriptionLabel: UILabel!


    @IBOutlet weak var infoTextField: UITextView!
    func configureView() {
        // Update the user interface for the detail item.
        if let detail = detailItem {
            if let label = detailDescriptionLabel {
                label.text = detail.funFact
            }
            if let text = infoTextField {
                let marks = db.marksFor(student: detail)
                text.text = "\(marks)"
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        configureView()
    }

    var db: DatabaseManager!

    var detailItem: Student? {
        didSet {
            // Update the view.
            configureView()
        }
    }


}

